#ifndef _IO_H
#define _IO_H

#include <stdint.h>

#ifdef ARDUINO
#include <Arduino.h>
#include <SPI.h>
#endif // ARDUINO

#ifdef ARDUINO
 #define DUMMY_BYTE 	(0x00)
 #define SER_BAUD       (9600)
 #define SPI_SPEED      (10000)
 #define SPI_ENDINNESS  (MSBFIRST)
 #define SPI_MODE       (SPI_MODE3)
#endif // ARDUINO

uint8_t SPI_rxByte(void);
void SPI_txByte(uint8_t out);

#ifdef ARDUINO
 void SPI_init(void);
 void SER_init(void);
 void SER_txByte(uint8_t out);
 uint8_t SER_rxByte(void);
 void SER_txString(char* out);
 void SER_txInt(uint32_t out);
#endif // ARDUINO

#endif // _IO_H




