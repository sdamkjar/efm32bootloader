#include "io.h"
#include "commands.h"
#include "xmodem.h"

#ifdef ARDUINO
#include <Arduino.h>
#else
#include <task.h>
#endif


uint8_t waitFor(uint8_t c, uint8_t retries, uint16_t wait)
{
  uint8_t i, reply;

  delay(wait);

  /* Try to get an echo over SPI */
  for( i = 0; i < retries; ++i )
  {
    reply = SPI_rxByte( );
    if (reply == c)
    {
      return ERROR_NOERR;
    } 
  }

  return ERROR_BADREPLY;
}


uint8_t ATHENA_flush(void)
{
  uint8_t i;

 #ifdef ARDUINO
  SER_txString("SENDING: FLUSH");
 #endif // ARDUINO

  for(i = 0 ; i < RETRIES_FLUSH ; i++)
  {
    /* Send command */
    SPI_txByte(COMMAND_FLUSH);

    if( waitFor(COMMAND_FLUSH,RETRIES_FLUSH,DELAY_FLUSH) == ERROR_NOERR )
    {

 #ifdef ARDUINO
       SER_txString("SUCCESS: FLUSH");
 #endif

      return ERROR_NOERR;
    }
  }

 #ifdef ARDUINO
       SER_txString("ERROR: FLUSH");
 #endif

  return ERROR_FLUSH;
}


uint8_t ATHENA_verify(uint32_t* result)
{
  uint8_t  i;
  uint32_t temp_result;

 #ifdef ARDUINO
  SER_txString("SENDING: VERIFY");
 #endif // ARDUINO

  /* Make sure the command loop is receiving */
  //ATHENA_flush();

  /* Send command */
  SPI_txByte(COMMAND_VERIFY);

  /* Try to get confirmation of a reset */
  if( waitFor(COMMAND_VERIFY,RETRIES_VERIFY,DELAY_VERIFY) == ERROR_NOERR )
  {

    for(i=0;i<sizeof(uint32_t);i++) {
      temp_result <<= 8;
      temp_result |= SPI_rxByte();
    }

    *result = temp_result;

 #ifdef ARDUINO
    SER_txString("SUCCESS: VERIFY");
    SER_txInt(temp_result);
 #endif // ARDUINO
    return ERROR_NOERR;
  }

 #ifdef ARDUINO
  SER_txString("ERROR: VERIFY");
 #endif // ARDUINO

  return ERROR_VERIFY;
}

uint8_t ATHENA_boot(void)
{
  uint8_t i;

 #ifdef ARDUINO
  SER_txString("SENDING: BOOT");
 #endif

  /* Make sure the command loop is receiving */
  //ATHENA_flush();
  /* Send 16 consecutive commands to avoid accidental boot */
  for ( i = 0 ; i < 16+RETRIES_BOOT ; i++ )
  {
    SPI_txByte(COMMAND_BOOT);
    if(waitFor(COMMAND_BOOT,NO_RETRY,NO_DELAY)==ERROR_NOERR)
    {
 #ifdef ARDUINO
      SER_txString("SUCCESS: STARTING BOOT");
 #endif
      break;
    }
    if(i == (16+RETRIES_BOOT - 1))
    {
 #ifdef ARDUINO
      SER_txString("ERROR: BOOT");
 #endif
 
      return ERROR_BOOT;
    }
  }
  /* Wait for echo from booted application */
  if( waitFor(COMMAND_BOOT,RETRIES_BOOT,DELAY_BOOT) == ERROR_NOERR )
  {
 #ifdef ARDUINO
    SER_txString("SUCCESS: BOOT");
 #endif
    return ERROR_NOERR;
  }  

 #ifdef ARDUINO
  SER_txString("ERROR: BOOT");
 #endif
 
  return ERROR_BOOT;
}


uint8_t ATHENA_reset(void)
{
 #ifdef ARDUINO
  SER_txString("SENDING: RESET");
 #endif

  /* Make sure the command loop is receiving */
  //ATHENA_flush();

  /* Send command */
  SPI_txByte(COMMAND_RESET);

  /* Try to get confirmation of a reset */
  if( waitFor(COMMAND_RESET,RETRIES_RESET,DELAY_RESET) == ERROR_NOERR )
  {
 #ifdef ARDUINO
    SER_txString("SUCCESS: RESET");
 #endif
    return ERROR_NOERR;
  }
  
 #ifdef ARDUINO
  SER_txString("ERROR: RESET");
 #endif    
 
  return ERROR_RESET;
}


uint8_t ATHENA_upload(uint8_t* binary, uint16_t binSizeInBytes)
{  
  uint8_t i;

 #ifdef ARDUINO
  SER_txString("SENDING: UPLOAD");
 #endif

  /* Make sure the command loop is receiving */
  //ATHENA_flush();

  /* Send 16 consecutive commands to avoid accidental upload */
  /* Get receipt of command before starting upload */
  for ( i = 0 ; i < 32 ; i++ )
  {
    SPI_txByte(COMMAND_UPLOAD);
    if(waitFor(COMMAND_UPLOAD,NO_RETRY,DELAY_U_RECEIPT)==ERROR_NOERR)
    {
      break;
    }
    if (i == 31){
 #ifdef ARDUINO
      SER_txString("ERROR: UPLOAD RECEIPT");
 #endif    
      return ERROR_U_RECEIPT;
    }
  }



 #ifdef ARDUINO
  SER_txString("SUCCESS: UPLOAD RECEIPT");
  SER_txString("Waiting for NCG.");
 #endif

  /* Get receipt of command before starting upload */
  if( waitFor(XMODEM_NCG,RETRIES_U_NCG,DELAY_U_NCG) != ERROR_NOERR )
  {
 #ifdef ARDUINO
    SER_txString("ERROR: UPLOAD NCG");
 #endif    
    return ERROR_U_NCG;
  }

 #ifdef ARDUINO
  SER_txString("SUCCESS: STARTING UPLOAD");
 #endif

  if( send_xmodem(binary,binSizeInBytes,1) == ERROR_NOERR )
  {
 #ifdef ARDUINO
    SER_txString("SUCCESS: UPLOAD");
 #endif
    return ERROR_NOERR;
  }

 #ifdef ARDUINO
  SER_txString("ERROR: UPLOAD");
 #endif

  return ERROR_U_XMSEND;
}


uint8_t ATHENA_download(uint8_t* data) 
{
  uint8_t i;

 #ifdef ARDUINO
  SER_txString("SENDING: DOWNLOAD");
 #endif

  /* Make sure the command loop is receiving */
  //ATHENA_flush();

  /* Send command */
  SPI_txByte(COMMAND_DOWNLOAD);

    /* Try to get confirmation of a reset */
  if( waitFor(COMMAND_DOWNLOAD,RETRIES_DOWNLOAD,DELAY_DOWNLOAD) == ERROR_NOERR )
  {
 #ifdef ARDUINO
    SER_txString("SUCCESS: DOWNLOAD");
 #endif

    for (i = 0 ; i < PACKET_SIZE ; i++){
      data[i] = SPI_rxByte();
    }

    return ERROR_NOERR;
  }
  
 #ifdef ARDUINO
  SER_txString("ERROR: DOWNLOAD");
 #endif    
 
  return ERROR_RESET;
}
