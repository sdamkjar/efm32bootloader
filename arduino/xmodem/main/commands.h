#ifndef _COMMANDS_H
#define _COMMANDS_H


#include <stdint.h>

/* SOH + PACKET_NO + (0xFF - PACKET_NO) + 128 Bytes + CRC16 */
#define PACKET_SIZE		   (133)

#define COMMAND_FLUSH      (0x4B)
#define COMMAND_VERIFY     (0x51)
#define COMMAND_BOOT       (0x68)
#define COMMAND_UPLOAD     (0x72)
#define COMMAND_DOWNLOAD   (0x7F)
#define COMMAND_RESET      (0x5C)

#define ERROR_NOERR		   (0x00)
#define ERROR_BADREPLY	   (0x0D)
#define ERROR_FLUSH		   (0x17)
#define ERROR_VERIFY	   (0x1A)
#define ERROR_BOOT		   (0x23)
#define ERROR_U_RECEIPT	   (0x2E)
#define ERROR_U_NCG		   (0x34)
#define ERROR_U_XMSEND	   (0x39)
#define ERROR_DOWNLOAD     (0x46)
#define ERROR_RESET        (0x65)

#define NO_RETRY       (1)
#define RETRIES_FLUSH	   (10)
#define RETRIES_VERIFY	   (10)
#define RETRIES_BOOT	   (10)
#define RETRIES_RESET	   (10)
#define RETRIES_DOWNLOAD   (10)
#define RETRIES_U_RECEIPT  (10)
#define RETRIES_U_NCG	   (10)
#define RETRIES_U_XMSEND   (10)

/* Delay in MILLISEC */
#define NO_DELAY       (0)
#define DELAY_FLUSH		   (100)
#define DELAY_VERIFY	   (2000)
#define DELAY_BOOT		   (1000)
#define DELAY_RESET		   (1000)
#define DELAY_DOWNLOAD     (NO_DELAY)
#define DELAY_U_RECEIPT	   (NO_DELAY)
#define DELAY_U_NCG		   (1000)

uint8_t ATHENA_flush(void);

uint8_t ATHENA_verify(uint32_t* result);

uint8_t ATHENA_boot(void);

uint8_t ATHENA_reset(void);

uint8_t ATHENA_upload(uint8_t* binary, uint16_t binSizeInBytes);

uint8_t ATHENA_download(uint8_t* data);


#endif // _COMMANDS_H
