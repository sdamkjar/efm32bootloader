#ifndef _XMODEM_H
#define _XMODEM_H

#include <stdint.h>

#define XMODEM_SOH                (1)
#define XMODEM_EOT                (4)
#define XMODEM_ACK                (6)
#define XMODEM_NAK                (21)
#define XMODEM_CAN                (24)
#define XMODEM_NCG                (67)

uint8_t send_xmodem(uint8_t *data, uint16_t binLength, uint8_t send_padding);

#endif // _XMODEM_H
