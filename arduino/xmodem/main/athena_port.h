/*
 * athena_port.h
 *
 *  Created on: Jun 7, 2016
 *      Author: bbruner
 */

#ifndef LIB_LIBCORE_ATHENA_SUBMODULE_ARDUINO_XMODEM_MAIN_ATHENA_PORT_H_
#define LIB_LIBCORE_ATHENA_SUBMODULE_ARDUINO_XMODEM_MAIN_ATHENA_PORT_H_

#ifndef ARDUINO

#include <portable_types.h>
#include <csp_internal.h>

/* Delay method */
#define delay(time) task_delay(time)

#endif


#endif /* LIB_LIBCORE_ATHENA_SUBMODULE_ARDUINO_XMODEM_MAIN_ATHENA_PORT_H_ */
