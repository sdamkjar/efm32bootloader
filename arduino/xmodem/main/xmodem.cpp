
#include "xmodem.h"
#include "commands.h"

#include "athena_port.h"
#include "io.h"


#define POLY 0x1021

void send_xmodem_packet_header(uint8_t packet_no)
{
  SPI_txByte(XMODEM_SOH);
  SPI_txByte(packet_no);
  SPI_txByte(255-packet_no);
  packet_no++;
}

/* send_xmodem function: takes care of all xmodem-related stuff
 * operates serially
 * arguments:
 *   data: the data to send - may contain zero bytes if you really want
 *   length: the number of bytes in the data
 *   send_padding: set this to 1 if you want to pad this out to a full packet
 *                 (probably set on the last packet to be sent)
 */
 //TODO: Add NAK detection and auto-retry
 //TODO: Add error handling (retry limit, timeout, etc.)
uint8_t send_xmodem(uint8_t *data, uint16_t binLength, uint8_t send_padding)
{
  uint8_t packet_no    = 1;
  static uint8_t bytes_sent   = 0;    /* Counts the bytes in each packet */
  static int16_t crc_register = 0;    /* Keeps track of the CRC */
  uint8_t padding             = 0;    /* Pad with spaces */
  int16_t nextBit;                    /* Used to compute CRC */
  uint8_t i;

#ifdef ARDUINO
  uint8_t ACK_flag = 1;
  uint8_t NAK_flag = 0;
#endif // ARDUINO

  if (binLength == 0 && send_padding && bytes_sent > 0) {
    binLength = 1;
    data = &padding;
  }
  while (binLength > 0) {
    if (bytes_sent == 0) {    /* Start of a packet */
      #ifdef ARDUINO
        if(ACK_flag == 0){
          SER_txString("Error: NAK received.");
          NAK_flag = 1;
        } 
        ACK_flag = 0;
      #endif // ARDUINO
      send_xmodem_packet_header(packet_no++);
    }

    /* Go through it byte-by-byte
       For each byte, add to the CRC and send it */
    while ((binLength > 0 || send_padding) && bytes_sent < 128) {
      for (nextBit = 7; nextBit >= 0; nextBit--) {
        /* Get out the MSB of the register */
        int do_xor;
        do_xor = (crc_register < 0);
        crc_register <<= 1;
        crc_register |= (*data >> nextBit) & 1;
        if (do_xor)  crc_register ^= POLY;
      }
      SPI_txByte(*data);
         
      bytes_sent++;
      if (binLength <= 1 && send_padding) {
        binLength = 0;
        data = &padding;
      }
      else {
        data++;    /* Move on to the next byte */
        binLength--;
      }
    }

    if (bytes_sent == 128) {
      /* End of packet; add 2 zero bytes to the CRC and send it */
      for (nextBit = 15; nextBit >= 0; nextBit--) {
        /* Get out the MSB of the register */
        int do_xor;
        do_xor = (crc_register < 0);
        crc_register <<= 1;
        /* Since we're adding zero bytes, there is no
           need to add any data bits to the register */
        if (do_xor)  crc_register ^= POLY;
      }
      SPI_txByte(crc_register >> 8 );
      SPI_txByte(crc_register & 0xFF);

      bytes_sent = 0;
      /* Reset static variables for the next packet */
      crc_register = 0;
    }

    for (i=0 ; i < RETRIES_U_XMSEND ; i++) {
      if (SPI_rxByte() == XMODEM_ACK) {
#ifdef ARDUINO
      ACK_flag = 1;
#endif // ARDUINO
        break;
      }
    }
  }
#ifdef ARDUINO
  if (NAK_flag) SER_txString("Error: Failed to upload binary.");
  else SER_txString("......Upload successful!");
#endif // ARDUINO
  return 0;
}
