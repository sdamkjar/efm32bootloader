#include "io.h"
#include <stdint.h>

#ifdef ARDUINO

int ss = 53;

void SPI_init(void)
{
  pinMode(ss, OUTPUT);
  SPI.begin(); // wake up the SPI bus.
  SPI.beginTransaction(SPISettings(SPI_SPEED, SPI_ENDINNESS, SPI_MODE));
  delay(500);
}

uint8_t SPI_rxByte(void)
{
  digitalWrite(ss, LOW);
  uint8_t in = SPI.transfer(DUMMY_BYTE);
  digitalWrite(ss, HIGH);
  return in;
}

void SPI_txByte(uint8_t out)
{
  digitalWrite(ss, LOW);
  SPI.transfer(out);
  digitalWrite(ss, HIGH);
}

void SER_init(void)
{
  // Open serial communications and wait for port to open:
  Serial.begin(SER_BAUD);
  while (!Serial) {
    ; // wait for serial port to connect.
  }
}

uint8_t SER_rxByte(void)
{
  uint8_t in;
  in = Serial.read();
  return in;
}

void SER_txByte(uint8_t out)
{
  Serial.write(out);
}

void SER_txString(char* out)
{
  Serial.write('\r');
  Serial.println(out);
}

void SER_txInt(uint32_t out)
{
  int digit;
  uint8_t i,c;
  for (i = 0; i <= 7; i++)
  {
    digit = out >> 28;
    c = digit + 0x30;
    if (digit >= 10)
    {
      c += 7;
    }
    SER_txByte(c);
    out <<= 4;
  }
  SER_txByte('\n');
}

#else // ARDUINO

uint8_t SPI_rxByte(void)
{
// TODO: add code for NanoMind
}

void SPI_txByte(uint8_t out)
{
// TODO: add code for NanoMind
}

#endif // ARDUINO
