/**************************************************************************//**
 * @file config.h
 * @brief Bootloader Configuration.
 *    This file defines how the bootloader is set up.
 * @author Silicon Labs
 * @version x.xx
 ******************************************************************************
 * @section License
 * <b>(C) Copyright 2014 Silicon Labs, http://www.silabs.com</b>
 *******************************************************************************
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 *
 * DISCLAIMER OF WARRANTY/LIMITATION OF REMEDIES: Silicon Labs has no
 * obligation to support this Software. Silicon Labs is providing the
 * Software "AS IS", with no express or implied warranties of any kind,
 * including, but not limited to, any implied warranties of merchantability
 * or fitness for any particular purpose or warranties against infringement
 * of any proprietary rights of a third party.
 *
 * Silicon Labs will not be liable for any consequential, incidental, or
 * special damages, or any other relief, or for any claim by any third party,
 * arising from your use of this Software.
 *
 ******************************************************************************/
#ifndef CONFIGURATION_H
#define CONFIGURATION_H

#include "em_device.h"

/******************************************************************************
 ** Frequency of the LF clock                                                 *
 ******************************************************************************/
#define LFRCO_FREQ           (32768)

/******************************************************************************
 * Number of seconds before autobaud times out and restarts the bootloader    *
 ******************************************************************************/
//#define AUTOBAUD_TIMEOUT     (30)

/******************************************************************************
 * Number of milliseconds between each consecutive polling of the SWD pins    *
 ******************************************************************************/
#define PIN_LOOP_INTERVAL    (250)

/******************************************************************************
 * The size of the bootloader flash image                                     *
 ******************************************************************************/
#define BOOTLOADER_SIZE      (4096)

/******************************************************************************
 * USART used for communication.                                              *
 ******************************************************************************/
#define BOOTLOADER_USART           USART2
#define BOOTLOADER_USART_CLOCKEN   CMU_HFPERCLKEN0_USART2
#define BOOTLOADER_USART_LOCATION  USART_ROUTE_LOCATION_LOC0

/******************************************************************************
 * USART used for debugging.                                                  *
 ******************************************************************************/
//#define DEBUG_USART                UART1
//#define DEBUG_USART_CLOCK          CMU_HFPERCLKEN0_UART1
//#define DEBUG_USART_LOCATION       UART_ROUTE_LOCATION_LOC2

/******************************************************************************
 * TIMERn is used for autobaud. The channel and location must match the       *
 * RX line of BOOTLOADER_USART for this to work properly.                     *
 ******************************************************************************/
//#define AUTOBAUD_TIMER_CHANNEL     0
//#define AUTOBAUD_TIMER             TIMER2
//#define AUTOBAUD_TIMER_LOCATION    TIMER_ROUTE_LOCATION_LOC4
//#define AUTOBAUD_TIMER_IRQn        TIMER0_IRQn
//#define AUTOBAUD_TIMER_CLOCK       CMU_HFPERCLKEN0_TIMER0

/******************************************************************************
 * Inline functions to setup GPIO for serialports.                            *
 ******************************************************************************/
//__STATIC_INLINE void CONFIG_DebugGpioSetup(void)
//{
  /* Avoid false start by setting output as high */
//  GPIO->P[1].DOUT = (1 << 9);
//  GPIO->P[1].MODEH = GPIO_P_MODEH_MODE9_PUSHPULL | GPIO_P_MODEH_MODE10_INPUT;
//}

__STATIC_INLINE void CONFIG_UsartGpioSetup(void)
{
  /* Use USART1 location 1
   * 1 : MOSI - Pin D0, MISO - Pin D1, CLK - Pin D2, CS - Pin D3
   * Configure GPIO pins LOCATION 1 as push pull (MISO)
   * and input (MOSI, CLK, CS)
   * To avoid false start, configure output as high
   */

  /* Pin PC2 is configured to Input enabled */
  GPIO->P[2].MODEL = (GPIO->P[2].MODEL & ~_GPIO_P_MODEL_MODE2_MASK)
          | GPIO_P_MODEL_MODE2_INPUT;

  /* Pin PC3 is configured to Push-pull */
  GPIO->P[2].MODEL = (GPIO->P[2].MODEL & ~_GPIO_P_MODEL_MODE3_MASK)
          | GPIO_P_MODEL_MODE3_PUSHPULL;

  /* Pin PC4 is configured to Input enabled */
  GPIO->P[2].MODEL = (GPIO->P[2].MODEL & ~_GPIO_P_MODEL_MODE4_MASK)
          | GPIO_P_MODEL_MODE4_INPUT;

  /* Pin PC5 is configured to Input enabled */
  GPIO->P[2].MODEL = (GPIO->P[2].MODEL & ~_GPIO_P_MODEL_MODE5_MASK)
          | GPIO_P_MODEL_MODE5_INPUT;

}

#endif
