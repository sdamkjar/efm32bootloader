/**************************************************************************//**
 * @file usart.c
 * @brief USART code for the EFM32 bootloader
 * @author Silicon Labs
 * @version x.xx
 ******************************************************************************
 * @section License
 * <b>(C) Copyright 2014 Silicon Labs, http://www.silabs.com</b>
 *******************************************************************************
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 *
 * DISCLAIMER OF WARRANTY/LIMITATION OF REMEDIES: Silicon Labs has no
 * obligation to support this Software. Silicon Labs is providing the
 * Software "AS IS", with no express or implied warranties of any kind,
 * including, but not limited to, any implied warranties of merchantability
 * or fitness for any particular purpose or warranties against infringement
 * of any proprietary rights of a third party.
 *
 * Silicon Labs will not be liable for any consequential, incidental, or
 * special damages, or any other relief, or for any claim by any third party,
 * arising from your use of this Software.
 *
 ******************************************************************************/

#include "em_device.h"
#include "usart.h"
#include "config.h"

/***************************************************************************//**
 * @brief
 *   Prints an int in hex.
 *
 * @param integer
 *   The integer to be printed.
 ******************************************************************************/
/*__ramfunc void USART_printHex(uint32_t integer)
{
  uint8_t c;
  int i, digit;

  for (i = 0; i <= 7; i++)
  {
    digit = integer >> 28;
    c = digit + 0x30;
    if (digit >= 10)
    {
      c += 7;
    }
    USART_txByte(c);
    integer <<= 4;
  }
}*/

/***************************************************************************//**
 * @brief
 *   Prints an int as 4 bytes.
 *
 * @param integer
 *   The integer to be printed.
 ******************************************************************************/
__ramfunc void USART_printInt(uint32_t integer)
{
  int i, digit;
  for (i = 0; i <= 4; i++)
  {
    digit = integer >> 24;
    USART_txByte(digit);
    integer <<= 8;
  }
}

/**************************************************************************//**
 * @brief Transmit single byte to BOOTLOADER_USART
 *****************************************************************************/
__ramfunc uint8_t USART_rxByte(void)
{
  uint32_t timer = 1000000;
  while (!(BOOTLOADER_USART->STATUS & USART_STATUS_RXDATAV) && --timer ) ;
  if (timer > 0)
  {
    return((uint8_t)(BOOTLOADER_USART->RXDATA & 0xFF));
  }
  else
  {
    return 0;
  }
}


/**************************************************************************//**
 * @brief Transmit single byte to BOOTLOADER_USART
 *****************************************************************************/
__ramfunc void USART_txByte(uint8_t data)
{
  /* Check that transmit buffer is empty */
  while (!(BOOTLOADER_USART->STATUS & USART_STATUS_TXBL)) ;

  BOOTLOADER_USART->TXDATA = (uint32_t) data;
}

/**************************************************************************//**
 * @brief Transmit null-terminated string to BOOTLOADER_USART
 *****************************************************************************/
/*__ramfunc void USART_printString(uint8_t *string)
{
  while (*string != 0)
  {
    USART_txByte(*string++);
  }
}*/

/**************************************************************************//**
 * @brief Intializes BOOTLOADER_USART
 *
 * @param clkdiv
 *   The clock divisor to use.
 *****************************************************************************/
void USART_init(void)
{
  /* Configure BOOTLOADER_USART */
  /* USART default to 1 stop bit, no parity, 8 data bits, so not
   * explicitly set */

  /* Init USART registers to HW reset state. */
  //USART_reset(BOOTLOADER_USART);

  USART_InitSync_TypeDef initsync = USART_INITSYNC_DEFAULT;

  initsync.baudrate = 115200;
  initsync.databits = usartDatabits8;
  initsync.master = 0;
  initsync.msbf = 1;
  initsync.clockMode = usartClockMode3;

  USART_setSync(BOOTLOADER_USART,&initsync);

  /* Cofnigure USART location */
  BOOTLOADER_USART->ROUTE = (BOOTLOADER_USART->ROUTE & ~_USART_ROUTE_LOCATION_MASK)
      | BOOTLOADER_USART_LOCATION;

  /* Enable signals CLK, CS, RX, TX */
  BOOTLOADER_USART->ROUTE |= USART_ROUTE_CLKPEN | USART_ROUTE_CSPEN | USART_ROUTE_RXPEN
      | USART_ROUTE_TXPEN;

  /* Clear RX/TX buffers */
  BOOTLOADER_USART->CMD = USART_CMD_CLEARRX | USART_CMD_CLEARTX;
}

/***************************************************************************//**
 * @brief
 *   Init USART for synchronous mode.
 *
 * @details
 *   This function will configure basic settings in order to operate in
 *   synchronous mode.
 *
 *   Special control setup not covered by this function must be done after
 *   using this function by direct modification of the CTRL register.
 *
 *   Notice that pins used by the USART module must be properly configured
 *   by the user explicitly, in order for the USART to work as intended.
 *   (When configuring pins, one should remember to consider the sequence of
 *   configuration, in order to avoid unintended pulses/glitches on output
 *   pins.)
 *
 * @param[in] usart
 *   Pointer to USART peripheral register block. (UART does not support this
 *   mode.)
 *
 * @param[in] init
 *   Pointer to initialization structure used to configure basic async setup.
 ******************************************************************************/
void USART_setSync(USART_TypeDef *usart, const USART_InitSync_TypeDef *init)
{

  
  /* Make sure disabled first, before resetting other registers */
  usart->CMD = USART_CMD_RXDIS | USART_CMD_TXDIS | USART_CMD_MASTERDIS
               | USART_CMD_RXBLOCKDIS | USART_CMD_TXTRIDIS | USART_CMD_CLEARTX
               | USART_CMD_CLEARRX;
  usart->CTRL      = _USART_CTRL_RESETVALUE;
  usart->FRAME     = _USART_FRAME_RESETVALUE;
  usart->TRIGCTRL  = _USART_TRIGCTRL_RESETVALUE;
  usart->CLKDIV    = _USART_CLKDIV_RESETVALUE;
  usart->IEN       = _USART_IEN_RESETVALUE;
  usart->IFC       = _USART_IFC_MASK;
#if defined(_USART_ROUTEPEN_MASK) || defined(_UART_ROUTEPEN_MASK)
  usart->ROUTEPEN  = _USART_ROUTEPEN_RESETVALUE;
  usart->ROUTELOC0 = _USART_ROUTELOC0_RESETVALUE;
  usart->ROUTELOC1 = _USART_ROUTELOC1_RESETVALUE;
#else
  usart->ROUTE     = _USART_ROUTE_RESETVALUE;
#endif

#if defined(_USART_IRCTRL_RESETVALUE)
  usart->IRCTRL = _USART_IRCTRL_RESETVALUE;
#endif

#if defined(_USART_INPUT_RESETVALUE)
  usart->INPUT = _USART_INPUT_RESETVALUE;
#endif

#if defined(_USART_I2SCTRL_RESETVALUE)
      usart->I2SCTRL = _USART_I2SCTRL_RESETVALUE;
#endif

  /* Set bits for synchronous mode */
  usart->CTRL |= (USART_CTRL_SYNC)
                 | (uint32_t)init->clockMode
                 | (init->msbf ? USART_CTRL_MSBF : 0);

#if defined(_USART_CTRL_AUTOTX_MASK)
  usart->CTRL |= init->autoTx ? USART_CTRL_AUTOTX : 0;
#endif

#if defined(_USART_INPUT_RXPRS_MASK)
  /* Configure PRS input mode. */
  if (init->prsRxEnable)
  {
    usart->INPUT = (uint32_t)init->prsRxCh | USART_INPUT_RXPRS;
  }
#endif

  /* Configure databits, leave stopbits and parity at reset default (not used) */
  usart->FRAME = (uint32_t)init->databits
                 | USART_FRAME_STOPBITS_DEFAULT
                 | USART_FRAME_PARITY_DEFAULT;

  /* Finally enable (as specified) */
  if (init->master)
  {
    usart->CMD = USART_CMD_MASTEREN;
  }

#if defined(_USART_TIMING_CSHOLD_MASK)
  usart->TIMING = ((init->autoCsHold << _USART_TIMING_CSHOLD_SHIFT)
                   & _USART_TIMING_CSHOLD_MASK)
                  | ((init->autoCsSetup << _USART_TIMING_CSSETUP_SHIFT)
                     & _USART_TIMING_CSSETUP_MASK);
  if (init->autoCsEnable)
  {
    usart->CTRL |= USART_CTRL_AUTOCS;
  }
#endif

  usart->CMD = (uint32_t)init->enable;


}
