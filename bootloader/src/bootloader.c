/**************************************************************************//**
 * @file bootloader.c
 * @brief EFM32 Bootloader. Preinstalled on all new EFM32 devices
 * @author Silicon Labs
 * @version x.xx
 ******************************************************************************
 * @section License
 * <b>(C) Copyright 2014 Silicon Labs, http://www.silabs.com</b>
 *******************************************************************************
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 *
 * DISCLAIMER OF WARRANTY/LIMITATION OF REMEDIES: Silicon Labs has no
 * obligation to support this Software. Silicon Labs is providing the
 * Software "AS IS", with no express or implied warranties of any kind,
 * including, but not limited to, any implied warranties of merchantability
 * or fitness for any particular purpose or warranties against infringement
 * of any proprietary rights of a third party.
 *
 * Silicon Labs will not be liable for any consequential, incidental, or
 * special damages, or any other relief, or for any claim by any third party,
 * arising from your use of this Software.
 *
 ******************************************************************************/

#include <stdbool.h>
#include "em_device.h"
#include "usart.h"
#include "xmodem.h"
#include "boot.h"
#include "crc.h"
#include "config.h"
#include "flash.h"
#include "brownout.h"

#define COMMAND_FLUSH      (0x4B)
#define COMMAND_VERIFY     (0x51)
#define COMMAND_BOOT       (0x68)
#define COMMAND_UPLOAD     (0x72)
#define COMMAND_DOWNLOAD   (0x7F)
#define COMMAND_RESET      (0x5C)

// #ifndef NDEBUG
// #include "debug.h"
// #include <stdio.h>
// #endif

/** Version string, used when the user connects */
// #define BOOTLOADER_VERSION_STRING "1.63 "

/** Version string, used when the user connects */
// #if !defined( BOOTLOADER_VERSION_STRING )
// #error "No bootloader version string defined !"
// #endif

/* Vector table in RAM. We construct a new vector table to conserve space in
 * flash as it is sparsly populated. */
#if defined (__ICCARM__)
#pragma location=0x20000000
__no_init uint32_t vectorTable[50];
#elif defined (__CC_ARM)
uint32_t vectorTable[50] __attribute__((at(0x20000000)));
#elif defined (__GNUC__)
uint32_t vectorTable[50] __attribute__((aligned(512)));
#else
#error Undefined toolkit, need to define alignment
#endif


/* If this flag is set the bootloader will be reset when the RTC expires.
 * This is used when autobaud is started. If there has been no synchronization
 * until the RTC expires the entire bootloader is reset.
 *
 * Essentially, this makes the RTC work as a watchdog timer.
 */
bool resetEFM32onRTCTimeout = false;

__ramfunc __noreturn void commandlineLoop(void);
__ramfunc void verify(uint32_t start, uint32_t end);


/**************************************************************************//**
 * @brief RTC IRQ Handler
 *   The RTC is used to keep the power consumption of the bootloader down while
 *   waiting for the pins to settle, or work as a watchdog in the autobaud
 *   sequence.
 *****************************************************************************/
void RTC_IRQHandler(void)
{
  /* Clear interrupt flag */
  RTC->IFC = RTC_IFC_COMP1 | RTC_IFC_COMP0 | RTC_IFC_OF;
}

/**************************************************************************//**
 * @brief
 *   Helper function to print flash write verification using CRC
 * @param start
 *   The start of the block to calculate CRC of.
 * @param end
 *   The end of the block. This byte is not included in the checksum.
 *****************************************************************************/
__ramfunc void verify(uint32_t start, uint32_t end)
{
  USART_printInt(CRC_calc((void *) start, (void *) end));
}

/**************************************************************************//**
 * @brief
 *   The main command line loop. Placed in Ram so that it can still run after
 *   a destructive write operation.
 *   NOTE: __ramfunc is a IAR specific instruction to put code into RAM.
 *   This allows the bootloader to survive a destructive upload.
 *****************************************************************************/
__ramfunc __noreturn void commandlineLoop(void)
{
  uint32_t flashSize;
  uint8_t  c;
  uint8_t  bootGuard = 0;
  uint8_t  uploadGuard = 0;
  
  //uint8_t *returnString;

  /* Find the size of the flash. DEVINFO->MSIZE is the
   * size in KB so left shift by 10. */
  flashSize = ((DEVINFO->MSIZE & _DEVINFO_MSIZE_FLASH_MASK) >> _DEVINFO_MSIZE_FLASH_SHIFT)
              << 10;

  /* The main command loop */
  while (1)
  {
    /* Retrieve new character */
    c = USART_rxByte();
    /* Echo */
    if (c != 0)
    {
      
      if ( c != COMMAND_BOOT   ) bootGuard   = 0;
      if ( c != COMMAND_UPLOAD ) uploadGuard = 0;
    }
    switch (c)
    {
    /* Upload command */
    case COMMAND_UPLOAD:
      
      //USART_printString(readyString);
      if (uploadGuard++ >= 16)
      {
        USART_txByte(c);
        XMODEM_download(BOOTLOADER_SIZE, flashSize);
      }
      break;
    /* Boot into new program */
    case COMMAND_BOOT:
      if (bootGuard++ >= 16)
      {
        USART_txByte(c);
        BOOT_boot();
      }
      break;
    /* Verify content by calculating CRC of entire flash */
    case COMMAND_VERIFY:
      USART_txByte(c);
      verify(0, flashSize);
      break;
    /* Reset command */
    case COMMAND_RESET:
      USART_txByte(c);
      /* Write to the Application Interrupt/Reset Command Register to reset
       * the EFM32. See section 9.3.7 in the reference manual. */
      SCB->AIRCR = 0x05FA0004;
      break;
    case COMMAND_FLUSH:
      USART_txByte(c);
      /* Do nothing */
      break;
    /* Unknown command */
    case 0:
      /* Timeout waiting for RX - avoid printing the unknown string. */
      break;
    default:
      /* Do nothing */
    }
  }
}

/**************************************************************************//**
 * @brief  Create a new vector table in RAM.
 *         We generate it here to conserve space in flash.
 *****************************************************************************/
static void generateVectorTable(void)
{
  vectorTable[RTC_IRQn + 16]            = (uint32_t) RTC_IRQHandler;
  vectorTable[VCMP_IRQn + 16]           = (uint32_t) VCMP_IRQHandler;
#ifdef USART_OVERLAPS_WITH_BOOTLOADER
  vectorTable[GPIO_EVEN_IRQn + 16]      = (uint32_t) GPIO_IRQHandler;
#endif
  SCB->VTOR                             = (uint32_t) vectorTable;
}

/**************************************************************************//**
 * @brief  Main function
 *****************************************************************************/
__noreturn void main(void)
{
  
  uint32_t tuning;

  /* Handle potential chip errata */
  /* Uncomment the next line to enable chip erratas for engineering samples */
  /* CHIP_init(); */

  /* Generate a new vector table and place it in RAM */
  generateVectorTable();

  /* Enable clocks for peripherals. */
  CMU->HFPERCLKDIV = CMU_HFPERCLKDIV_HFPERCLKEN;
  CMU->HFPERCLKEN0 = CMU_HFPERCLKEN0_GPIO | BOOTLOADER_USART_CLOCKEN;

  /* Enable LE and DMA interface */
  CMU->HFCORECLKEN0 = CMU_HFCORECLKEN0_LE | CMU_HFCORECLKEN0_DMA;

  /* Enable LFRCO for RTC */
  CMU->OSCENCMD = CMU_OSCENCMD_LFRCOEN;
  /* Setup LFA to use LFRCRO */
  CMU->LFCLKSEL = CMU_LFCLKSEL_LFA_LFRCO | CMU_LFCLKSEL_LFB_HFCORECLKLEDIV2;
  /* Enable RTC */
  CMU->LFACLKEN0 = CMU_LFACLKEN0_RTC;

  /* Wait for a boot operation */
  //waitForBootOrUSART();

// #if defined( BOOTLOADER_LEUART_CLOCKEN )
//   /* Enable LEUART */
//   CMU->LFBCLKEN0 = BOOTLOADER_LEUART_CLOCKEN;
// #endif

#if defined ( _DEVINFO_HFRCOCAL1_BAND28_MASK )
  /* Change to 28MHz internal osciallator to increase speed of
   * bootloader */
  tuning = (DEVINFO->HFRCOCAL1 & _DEVINFO_HFRCOCAL1_BAND28_MASK)
           >> _DEVINFO_HFRCOCAL1_BAND28_SHIFT;

  CMU->HFRCOCTRL = CMU_HFRCOCTRL_BAND_28MHZ | tuning;
#ifndef NDEBUG
  /* Calculate new clock division based on the 28Mhz clock */
  DEBUG_USART->CLKDIV = 3634;
#endif

#elif defined( _DEVINFO_HFRCOCAL1_BAND21_MASK )
  /* Change to 21MHz internal osciallator to increase speed of
   * bootloader */
  tuning = ((DEVINFO->HFRCOCAL1 & _DEVINFO_HFRCOCAL1_BAND21_MASK)
           >> _DEVINFO_HFRCOCAL1_BAND21_SHIFT);

  CMU->HFRCOCTRL = CMU_HFRCOCTRL_BAND_21MHZ | tuning;
#ifndef NDEBUG
  /* Calculate new clock division based on the 21Mhz clock */
  DEBUG_USART->CLKDIV = 2661;
#endif

#else
#error "Can not make correct clock selection."
#endif

  /* Setup pins for USART */
  CONFIG_UsartGpioSetup();

    /* When autobaud has completed, we can be fairly certain that
   * the entry into the bootloader is intentional so we can disable the timeout.
   */
  NVIC_DisableIRQ(RTC_IRQn);
  
  /* Enable VCMP and wait for warm-up complete */
  BRNO_Setup();
  BRNO_WaitForVoltageAboveTrigger();
  BRNO_armReset();

  /* Initialize the UART */
  USART_init();
  
  
  /* Echo reset */
  USART_txByte(COMMAND_RESET);


  /* Initialize flash for writing */
  FLASH_init();

  /* Start executing command line */
  commandlineLoop();
}
