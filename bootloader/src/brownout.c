/*
 * Copyright (C) 2016  Stefan Damkjar
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */


/**
 * @file brownout.c
 * @author Stefan Damkjar
 * @date 2015-06-28
 */

/*******************************************************************************
 *******************************  INCLUDES   ***********************************
 ******************************************************************************/

/* Driver Includes */
#include "brownout.h"

#include "stdint.h"
#include "stdbool.h"

/* EFM32 EMLIB Includes */
#ifndef BOOTLOADER
#include "em_emu.h"
#include "em_cmu.h"
#include "em_vcmp.h"
#else
#include "em_device.h"
#include "system_efm32gg.h"
#endif

/*******************************************************************************
 *******************************   DEFINES   ***********************************
 ******************************************************************************/

#ifdef BOOTLOADER

/* Consistency check, since restoring assumes similar bitpositions in */
/* CMU OSCENCMD and STATUS regs */
 #if (CMU_STATUS_AUXHFRCOENS != CMU_OSCENCMD_AUXHFRCOEN)
 #error Conflict in AUXHFRCOENS and AUXHFRCOEN bitpositions
 #endif
 #if (CMU_STATUS_HFXOENS != CMU_OSCENCMD_HFXOEN)
 #error Conflict in HFXOENS and HFXOEN bitpositions
 #endif
 #if (CMU_STATUS_LFRCOENS != CMU_OSCENCMD_LFRCOEN)
 #error Conflict in LFRCOENS and LFRCOEN bitpositions
 #endif
 #if (CMU_STATUS_LFXOENS != CMU_OSCENCMD_LFXOEN)
 #error Conflict in LFXOENS and LFXOEN bitpositions
 #endif


/** @cond DO_NOT_INCLUDE_WITH_DOXYGEN */
/* Fix for errata EMU_E107 - non-WIC interrupt masks. */
 #if defined( _EFM32_GECKO_FAMILY )
 #define ERRATA_FIX_EMU_E107_EN
 #define NON_WIC_INT_MASK_0    (~(0x0dfc0323U))
 #define NON_WIC_INT_MASK_1    (~(0x0U))

 #elif defined( _EFM32_TINY_FAMILY )
 #define ERRATA_FIX_EMU_E107_EN
 #define NON_WIC_INT_MASK_0    (~(0x001be323U))
 #define NON_WIC_INT_MASK_1    (~(0x0U))

 #elif defined( _EFM32_GIANT_FAMILY )
 #define ERRATA_FIX_EMU_E107_EN
 #define NON_WIC_INT_MASK_0    (~(0xff020e63U))
 #define NON_WIC_INT_MASK_1    (~(0x00000046U))

 #elif defined( _EFM32_WONDER_FAMILY )
 #define ERRATA_FIX_EMU_E107_EN
 #define NON_WIC_INT_MASK_0    (~(0xff020e63U))
 #define NON_WIC_INT_MASK_1    (~(0x00000046U))

 #else
/* Zero Gecko and future families are not affected by errata EMU_E107 */
 #endif

/* Fix for errata EMU_E108 - High Current Consumption on EM4 Entry. */
 #if defined( _EFM32_HAPPY_FAMILY )
 #define ERRATA_FIX_EMU_E108_EN
 #endif
/** @endcond */


 #if defined( _EMU_DCDCCTRL_MASK )
/* DCDCTODVDD output range min/max */
 #define PWRCFG_DCDCTODVDD_VMIN          1200
 #define PWRCFG_DCDCTODVDD_VMAX          3000
typedef enum
{
  errataFixDcdcHsInit,
  errataFixDcdcHsTrimSet,
  errataFixDcdcHsLnWaitDone
} errataFixDcdcHs_TypeDef;
errataFixDcdcHs_TypeDef errataFixDcdcHsState = errataFixDcdcHsInit;
 #endif

#endif

/*******************************************************************************
 ********************************   ENUMS   ************************************
 ******************************************************************************/

#ifdef BOOTLOADER

/** Warm-up Time in High Frequency Peripheral Clock cycles */
typedef enum
{
  /** 4 cycles */
  vcmpWarmTime4Cycles   = _VCMP_CTRL_WARMTIME_4CYCLES,
  /** 8 cycles */
  vcmpWarmTime8Cycles   = _VCMP_CTRL_WARMTIME_8CYCLES,
  /** 16 cycles */
  vcmpWarmTime16Cycles  = _VCMP_CTRL_WARMTIME_16CYCLES,
  /** 32 cycles */
  vcmpWarmTime32Cycles  = _VCMP_CTRL_WARMTIME_32CYCLES,
  /** 64 cycles */
  vcmpWarmTime64Cycles  = _VCMP_CTRL_WARMTIME_64CYCLES,
  /** 128 cycles */
  vcmpWarmTime128Cycles = _VCMP_CTRL_WARMTIME_128CYCLES,
  /** 256 cycles */
  vcmpWarmTime256Cycles = _VCMP_CTRL_WARMTIME_256CYCLES,
  /** 512 cycles */
  vcmpWarmTime512Cycles = _VCMP_CTRL_WARMTIME_512CYCLES
} VCMP_WarmTime_TypeDef;

/** Hyseresis configuration */
typedef enum
{
  /** Normal operation, no hysteresis */
  vcmpHystNone,
  /** Digital output will not toggle until positive edge is at least
   *  20mV above or below negative input voltage */
  vcmpHyst20mV
} VCMP_Hysteresis_TypeDef;

#endif

/*******************************************************************************
 *******************************   STRUCTS   ***********************************
 ******************************************************************************/

#ifdef BOOTLOADER

/** VCMP Initialization structure */
typedef struct
{
  /** If set to true, will reduce by half the bias current */
  bool                    halfBias;
  /** BIAS current configuration, depends on halfBias setting,
   *  above, see reference manual */
  int                     biasProg;
  /** Enable interrupt for falling edge */
  bool                    irqFalling;
  /** Enable interrupt for rising edge */
  bool                    irqRising;
  /** Warm-up time in clock cycles */
  VCMP_WarmTime_TypeDef   warmup;
  /** Hysteresis configuration */
  VCMP_Hysteresis_TypeDef hyst;
  /** Output value when comparator is inactive, should be 0 or 1 */
  int                     inactive;
  /** Enable low power mode for VDD and bandgap reference */
  bool                    lowPowerRef;
  /** Trigger level, according to formula
   *  VDD Trigger Level = 1.667V + 0.034V x triggerLevel */
  int                     triggerLevel;
  /** Enable VCMP after configuration */
  bool                    enable;
} VCMP_Init_TypeDef;

/** Default VCMP initialization structure */
#define VCMP_INIT_DEFAULT                                                \
{                                                                        \
  true,                /** Half Bias enabled */                          \
  0x7,                 /** Bias curernt 0.7 uA when half bias enabled */ \
  false,               /** Falling edge sense not enabled */             \
  false,               /** Rising edge sense not enabled */              \
  vcmpWarmTime4Cycles, /** 4 clock cycles warm-up time */                \
  vcmpHystNone,        /** No hysteresis */                              \
  0,                   /** 0 in digital ouput when inactive */           \
  true,                /** Do not use low power reference */             \
  39,                  /** Trigger level just below 3V */                \
  true,                /** Enable after init */                          \
}

#endif

/*******************************************************************************
 *******************************   GLOBALS   ***********************************
 ******************************************************************************/

static volatile bool resetArmed = false;

#ifdef BOOTLOADER

static uint32_t cmuStatus;
 #if defined( _CMU_HFCLKSTATUS_RESETVALUE )
static uint16_t cmuHfclkStatus;
 #endif

#endif

/*******************************************************************************
 ****************************  ERRATA FUNCTIONS   ******************************
 ******************************************************************************/

#ifdef BOOTLOADER

 #if defined( ERRATA_FIX_EMU_E107_EN )
/* Get enable conditions for errata EMU_E107 fix. */
__STATIC_INLINE bool getErrataFixEmuE107En(void)
{
  /* SYSTEM_ChipRevisionGet could have been used here, but we would like a
   * faster implementation in this case.
   */
  uint16_t majorMinorRev;

  /* CHIP MAJOR bit [3:0] */
  majorMinorRev = ((ROMTABLE->PID0 & _ROMTABLE_PID0_REVMAJOR_MASK)
                   >> _ROMTABLE_PID0_REVMAJOR_SHIFT)
                  << 8;
  /* CHIP MINOR bit [7:4] */
  majorMinorRev |= ((ROMTABLE->PID2 & _ROMTABLE_PID2_REVMINORMSB_MASK)
                    >> _ROMTABLE_PID2_REVMINORMSB_SHIFT)
                   << 4;
  /* CHIP MINOR bit [3:0] */
  majorMinorRev |= (ROMTABLE->PID3 & _ROMTABLE_PID3_REVMINORLSB_MASK)
                   >> _ROMTABLE_PID3_REVMINORLSB_SHIFT;

 #if defined( _EFM32_GECKO_FAMILY )
  return (majorMinorRev <= 0x0103);
 #elif defined( _EFM32_TINY_FAMILY )
  return (majorMinorRev <= 0x0102);
 #elif defined( _EFM32_GIANT_FAMILY )
  return (majorMinorRev <= 0x0103) || (majorMinorRev == 0x0204);
 #elif defined( _EFM32_WONDER_FAMILY )
  return (majorMinorRev == 0x0100);
 #else
  /* Zero Gecko and future families are not affected by errata EMU_E107 */
  return false;
 #endif
}
 #endif

 #if defined( _EMU_DCDCCTRL_MASK )
/* LP prepare / LN restore P/NFET count */
static void currentLimitersUpdate(void);
 #define DCDC_LP_PFET_CNT        7
 #define DCDC_LP_NFET_CNT        7
static void dcdcFetCntSet(bool lpModeSet)
{
  uint32_t tmp;
  static uint32_t emuDcdcMiscCtrlReg;

  if (lpModeSet)
  {
    emuDcdcMiscCtrlReg = EMU->DCDCMISCCTRL;
    tmp  = EMU->DCDCMISCCTRL
           & ~(_EMU_DCDCMISCCTRL_PFETCNT_MASK | _EMU_DCDCMISCCTRL_NFETCNT_MASK);
    tmp |= (DCDC_LP_PFET_CNT << _EMU_DCDCMISCCTRL_PFETCNT_SHIFT)
            | (DCDC_LP_NFET_CNT << _EMU_DCDCMISCCTRL_NFETCNT_SHIFT);
    EMU->DCDCMISCCTRL = tmp;
    currentLimitersUpdate();
  }
  else
  {
    EMU->DCDCMISCCTRL = emuDcdcMiscCtrlReg;
    currentLimitersUpdate();
  }
}

static void dcdcHsFixLnBlock(void)
{
 #define EMU_DCDCSTATUS  (* (volatile uint32_t *)(EMU_BASE + 0x7C))
  if (errataFixDcdcHsState == errataFixDcdcHsTrimSet)
  {
    /* Wait for LNRUNNING */
    if ((EMU->DCDCCTRL & _EMU_DCDCCTRL_DCDCMODE_MASK) == EMU_DCDCCTRL_DCDCMODE_LOWNOISE)
    {
      while (!(EMU_DCDCSTATUS & (0x1 << 16)));
    }
    errataFixDcdcHsState = errataFixDcdcHsLnWaitDone;
  }
}
 #endif

#endif

/*******************************************************************************
 ***************************   LOCAL FUNCTIONS   *******************************
 ******************************************************************************/

#ifdef BOOTLOADER

/***************************************************************************//**
 * @brief
 *   Configure and enable Voltage Comparator
 *
 * @param[in] vcmpInit
 *   VCMP Initialization structure
 ******************************************************************************/
void VCMP_Init(void)
{

  VCMP->CTRL = 0x40030604;

  /* Configure trigger level */
  /* Set trigger level */
  VCMP->INPUTSEL = (VCMP->INPUTSEL & ~(_VCMP_INPUTSEL_TRIGLEVEL_MASK))
                   | (TRIGGER_LEVEL << _VCMP_INPUTSEL_TRIGLEVEL_SHIFT);

  /* Initialize VCMP clock */
  CMU->HFPERCLKEN0 |= CMU_HFPERCLKEN0_VCMP;

  /* Clear edge interrupt */
  VCMP->IFC = VCMP_IF_EDGE;
}

/***************************************************************************//**
 * @brief
 *   Restore oscillators and core clock after having been in EM2 or EM3.
 ******************************************************************************/
static void emuRestore(void)
{
  uint32_t oscEnCmd;
  uint32_t cmuLocked;

  /* Although we could use the CMU API for most of the below handling, we */
  /* would like this function to be as efficient as possible. */

  /* CMU registers may be locked */
  cmuLocked = CMU->LOCK & CMU_LOCK_LOCKKEY_LOCKED;
  CMU->LOCK = CMU_LOCK_LOCKKEY_UNLOCK;

  /* AUXHFRCO are automatically disabled (except if using debugger). */
  /* HFRCO, USHFRCO and HFXO are automatically disabled. */
  /* LFRCO/LFXO may be disabled by SW in EM3. */
  /* Restore according to status prior to entering energy mode. */
  oscEnCmd = 0;
  oscEnCmd |= ((cmuStatus & CMU_STATUS_HFRCOENS)    ? CMU_OSCENCMD_HFRCOEN : 0);
  oscEnCmd |= ((cmuStatus & CMU_STATUS_AUXHFRCOENS) ? CMU_OSCENCMD_AUXHFRCOEN : 0);
  oscEnCmd |= ((cmuStatus & CMU_STATUS_LFRCOENS)    ? CMU_OSCENCMD_LFRCOEN : 0);
  oscEnCmd |= ((cmuStatus & CMU_STATUS_HFXOENS)     ? CMU_OSCENCMD_HFXOEN : 0);
  oscEnCmd |= ((cmuStatus & CMU_STATUS_LFXOENS)     ? CMU_OSCENCMD_LFXOEN : 0);
 #if defined( _CMU_STATUS_USHFRCOENS_MASK )
  oscEnCmd |= ((cmuStatus & CMU_STATUS_USHFRCOENS)  ? CMU_OSCENCMD_USHFRCOEN : 0);
 #endif
  CMU->OSCENCMD = oscEnCmd;


 #if defined( _CMU_HFCLKSTATUS_RESETVALUE )
  /* Restore oscillator used for clocking core */
  switch (cmuHfclkStatus & _CMU_HFCLKSTATUS_SELECTED_MASK)
  {
    case CMU_HFCLKSTATUS_SELECTED_LFRCO:
      /* HFRCO could only be selected if the autostart HFXO feature is not
       * enabled, otherwise the HFXO would be started and selected automatically.
       * Note: this error hook helps catching erroneous oscillator configurations,
       * when the AUTOSTARTSELEM0EM1 is set in CMU_HFXOCTRL. */
      if (!(CMU->HFXOCTRL & CMU_HFXOCTRL_AUTOSTARTSELEM0EM1))
      {
        /* Wait for LFRCO to stabilize */
        while (!(CMU->STATUS & CMU_STATUS_LFRCORDY))
          ;
        CMU->HFCLKSEL = CMU_HFCLKSEL_HF_LFRCO;
      }
      break;

    case CMU_HFCLKSTATUS_SELECTED_LFXO:
      /* Wait for LFXO to stabilize */
      while (!(CMU->STATUS & CMU_STATUS_LFXORDY))
        ;
      CMU->HFCLKSEL = CMU_HFCLKSEL_HF_LFXO;
      break;

    case CMU_HFCLKSTATUS_SELECTED_HFXO:
      /* Wait for HFXO to stabilize */
      while (!(CMU->STATUS & CMU_STATUS_HFXORDY))
        ;
      CMU->HFCLKSEL = CMU_HFCLKSEL_HF_HFXO;
      break;

    default: /* CMU_HFCLKSTATUS_SELECTED_HFRCO */
      /* If core clock was HFRCO core clock, it is automatically restored to */
      /* state prior to entering energy mode. No need for further action. */
      break;
  }
 #else
  switch (cmuStatus & (CMU_STATUS_HFRCOSEL
                      | CMU_STATUS_HFXOSEL
                      | CMU_STATUS_LFRCOSEL
 #if defined( CMU_STATUS_USHFRCODIV2SEL )
                      | CMU_STATUS_USHFRCODIV2SEL
 #endif
                      | CMU_STATUS_LFXOSEL))
  {
    case CMU_STATUS_LFRCOSEL:
      /* Wait for LFRCO to stabilize */
      while (!(CMU->STATUS & CMU_STATUS_LFRCORDY))
        ;
      CMU->CMD = CMU_CMD_HFCLKSEL_LFRCO;
      break;

    case CMU_STATUS_LFXOSEL:
      /* Wait for LFXO to stabilize */
      while (!(CMU->STATUS & CMU_STATUS_LFXORDY))
        ;
      CMU->CMD = CMU_CMD_HFCLKSEL_LFXO;
      break;

    case CMU_STATUS_HFXOSEL:
      /* Wait for HFXO to stabilize */
      while (!(CMU->STATUS & CMU_STATUS_HFXORDY))
        ;
      CMU->CMD = CMU_CMD_HFCLKSEL_HFXO;
      break;

 #if defined( CMU_STATUS_USHFRCODIV2SEL )
    case CMU_STATUS_USHFRCODIV2SEL:
      /* Wait for USHFRCO to stabilize */
      while (!(CMU->STATUS & CMU_STATUS_USHFRCORDY))
        ;
      CMU->CMD = _CMU_CMD_HFCLKSEL_USHFRCODIV2;
      break;
 #endif

    default: /* CMU_STATUS_HFRCOSEL */
      /* If core clock was HFRCO core clock, it is automatically restored to */
      /* state prior to entering energy mode. No need for further action. */
      break;
  }

  /* If HFRCO was disabled before entering Energy Mode, turn it off again */
  /* as it is automatically enabled by wake up */
  if ( ! (cmuStatus & CMU_STATUS_HFRCOENS) )
  {
    CMU->OSCENCMD = CMU_OSCENCMD_HFRCODIS;
  }
 #endif
  /* Restore CMU register locking */
  if (cmuLocked)
  {
    CMU->LOCK = CMU_LOCK_LOCKKEY_LOCK;
  }
}

void EMU_EnterEM3(bool restore)
{

  uint32_t cmuLocked;

 #if defined( ERRATA_FIX_EMU_E107_EN )
  bool errataFixEmuE107En;
  uint32_t nonWicIntEn[2];
 #endif

  /* Auto-update CMU status before entering energy mode. */
  cmuStatus = CMU->STATUS;
 #if defined( _CMU_HFCLKSTATUS_RESETVALUE )
  cmuHfclkStatus = (uint16_t)(CMU->HFCLKSTATUS);
 #endif

  /* CMU registers may be locked */
  cmuLocked = CMU->LOCK & CMU_LOCK_LOCKKEY_LOCKED;  
  CMU->LOCK = CMU_LOCK_LOCKKEY_UNLOCK;

  /* Disable LF oscillators */
  CMU->OSCENCMD = CMU_OSCENCMD_LFXODIS | CMU_OSCENCMD_LFRCODIS;

  /* Restore CMU register locking */
  if (cmuLocked)
  {
    CMU->LOCK = CMU_LOCK_LOCKKEY_LOCK;
  }

  /* Enter Cortex deep sleep mode */
  SCB->SCR |= SCB_SCR_SLEEPDEEP_Msk;

  /* Fix for errata EMU_E107 - store non-WIC interrupt enable flags.
     Disable the enabled non-WIC interrupts. */
 #if defined( ERRATA_FIX_EMU_E107_EN )
  errataFixEmuE107En = getErrataFixEmuE107En();
  if (errataFixEmuE107En)
  {
    nonWicIntEn[0] = NVIC->ISER[0] & NON_WIC_INT_MASK_0;
    NVIC->ICER[0] = nonWicIntEn[0];
 #if (NON_WIC_INT_MASK_1 != (~(0x0U)))
    nonWicIntEn[1] = NVIC->ISER[1] & NON_WIC_INT_MASK_1;
    NVIC->ICER[1] = nonWicIntEn[1];
 #endif

  }
 #endif

 #if defined( _EMU_DCDCCTRL_MASK )
  dcdcFetCntSet(true);
  dcdcHsFixLnBlock();
 #endif

  __WFI();

 #if defined( _EMU_DCDCCTRL_MASK )
  dcdcFetCntSet(false);
 #endif

  /* Fix for errata EMU_E107 - restore state of non-WIC interrupt enable flags. */
 #if defined( ERRATA_FIX_EMU_E107_EN )
  if (errataFixEmuE107En)
  {
    NVIC->ISER[0] = nonWicIntEn[0];
 #if (NON_WIC_INT_MASK_1 != (~(0x0U)))
    NVIC->ISER[1] = nonWicIntEn[1];
 #endif
  }
 #endif

  emuRestore();
 
}

#endif

void BRNO_armReset(void)
{
  resetArmed = true;
}

void BRNO_WaitForVoltageAboveTrigger(void)
{

  static uint32_t HFPERCLK_state = 0;

  uint32_t tuning;

  while(1)
  {
    /* Save the state of HF peripheral clock register */
    if ( !HFPERCLK_state )
    {
      HFPERCLK_state = CMU->HFPERCLKEN0;
    }

    /* Wait for VCMP reading to be ready */
    while (!(VCMP->STATUS & VCMP_STATUS_VCMPACT));

    /* If voltage is okay, restore HFPERCLK and exit the loop */
    if ( VCMP->STATUS & VCMP_STATUS_VCMPOUT )
    {
      CMU->HFPERCLKEN0 = HFPERCLK_state;
      break;
    }

    /* If voltage is low, disable all HFPERCLK except VCMP and loop
     * until voltage is okay */
    else
    {
      
      /* Disable all excess clocks */
      CMU->HFCORECLKEN0 = CMU_HFCORECLKEN0_LE;
      CMU->HFPERCLKEN0  = CMU_HFPERCLKEN0_VCMP;

      tuning = (DEVINFO->HFRCOCAL0 & _DEVINFO_HFRCOCAL0_BAND7_MASK)
                 >> _DEVINFO_HFRCOCAL0_BAND7_SHIFT;

      CMU->HFRCOCTRL = CMU_HFRCOCTRL_BAND_7MHZ | tuning;

      EMU_EnterEM3(true);
    }
  }
}

/***************************************************************************//**
 * @brief Initialize VCMP and enable interrupts
 ******************************************************************************/
void BRNO_Setup()
{

#ifndef BOOTLOADER
  /* Declare VCMP Init struct */
  VCMP_Init_TypeDef vcmp =
  {
    true,                               /* Half bias current */
    0,                                  /* Bias current configuration */
    true,                               /* Enable interrupt for falling edge */
    true,                               /* Enable interrupt for rising edge */
    vcmpWarmTime256Cycles,              /* Warm-up time in clock cycles */
    vcmpHystNone,                       /* Hysteresis configuration */
    1,                                  /* Inactive comparator output value */
    true,                               /* Enable low power mode */
    TRIGGER_LEVEL,                      /* Trigger level */
    false                               /* Enable VCMP after configuration */
  };

  /* Initialize VCMP clock */
  CMU->HFPERCLKEN0 |= CMU_HFPERCLKEN0_VCMP;
  VCMP_Init(&vcmp);
#else
  VCMP_Init();
#endif


  /* Enable VCMP interrupt lines */
  /* WARMUP interrupt is used to catch triggers during initialization */
  NVIC_EnableIRQ(VCMP_IRQn);
  VCMP->IEN |= VCMP_IEN_EDGE | VCMP_IEN_WARMUP;

  /* Enable VCMP and wait for warm-up complete */
  VCMP->CTRL |= VCMP_CTRL_EN;
  
  /* Enable Low Power Reference setting */
  VCMP->INPUTSEL |= VCMP_INPUTSEL_LPREF;
}

/*******************************************************************************
 **********************   INTERUPT SERVICE ROUTINES   **************************
 ******************************************************************************/

/***************************************************************************//**
 * @brief VCMP interrupt handler, triggers on EDGE and WARMUP events
 ******************************************************************************/
void VCMP_IRQHandler()
{

  /* Reset if brown-out is detected */
  if(resetArmed)
  {
    SCB->AIRCR = 0x05FA0004;
  }

  /* Clear interrupt flag */
  VCMP->IFC = _VCMP_IFC_MASK;

  /* Flushing instructions to make sure that the interrupt is not re-triggered*/
  /* This may be required when the peripheral clock is slower than the core */
  __DSB();
}
