/**************************************************************************//**
 * @file usart.h
 * @brief USART code for the EFM32 bootloader
 * @author Silicon Labs
 * @version x.xx
 ******************************************************************************
 * @section License
 * <b>(C) Copyright 2014 Silicon Labs, http://www.silabs.com</b>
 *******************************************************************************
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 *
 * DISCLAIMER OF WARRANTY/LIMITATION OF REMEDIES: Silicon Labs has no
 * obligation to support this Software. Silicon Labs is providing the
 * Software "AS IS", with no express or implied warranties of any kind,
 * including, but not limited to, any implied warranties of merchantability
 * or fitness for any particular purpose or warranties against infringement
 * of any proprietary rights of a third party.
 *
 * Silicon Labs will not be liable for any consequential, incidental, or
 * special damages, or any other relief, or for any claim by any third party,
 * arising from your use of this Software.
 *
 ******************************************************************************/

#ifndef _UART_H
#define _UART_H

#include <stdint.h>
#include <stdbool.h>
#include "em_device.h"
#include "compiler.h"

/*******************************************************************************
 ********************************   ENUMS   ************************************
 ******************************************************************************/

/** Databit selection. */
typedef enum
{
  usartDatabits4  = USART_FRAME_DATABITS_FOUR,     /**< 4 databits (not available for UART). */
  usartDatabits5  = USART_FRAME_DATABITS_FIVE,     /**< 5 databits (not available for UART). */
  usartDatabits6  = USART_FRAME_DATABITS_SIX,      /**< 6 databits (not available for UART). */
  usartDatabits7  = USART_FRAME_DATABITS_SEVEN,    /**< 7 databits (not available for UART). */
  usartDatabits8  = USART_FRAME_DATABITS_EIGHT,    /**< 8 databits. */
  usartDatabits9  = USART_FRAME_DATABITS_NINE,     /**< 9 databits. */
  usartDatabits10 = USART_FRAME_DATABITS_TEN,      /**< 10 databits (not available for UART). */
  usartDatabits11 = USART_FRAME_DATABITS_ELEVEN,   /**< 11 databits (not available for UART). */
  usartDatabits12 = USART_FRAME_DATABITS_TWELVE,   /**< 12 databits (not available for UART). */
  usartDatabits13 = USART_FRAME_DATABITS_THIRTEEN, /**< 13 databits (not available for UART). */
  usartDatabits14 = USART_FRAME_DATABITS_FOURTEEN, /**< 14 databits (not available for UART). */
  usartDatabits15 = USART_FRAME_DATABITS_FIFTEEN,  /**< 15 databits (not available for UART). */
  usartDatabits16 = USART_FRAME_DATABITS_SIXTEEN   /**< 16 databits (not available for UART). */
} USART_Databits_TypeDef;

/** Enable selection. */
typedef enum
{
  /** Disable both receiver and transmitter. */
  usartDisable  = 0x0,

  /** Enable receiver only, transmitter disabled. */
  usartEnableRx = USART_CMD_RXEN,

  /** Enable transmitter only, receiver disabled. */
  usartEnableTx = USART_CMD_TXEN,

  /** Enable both receiver and transmitter. */
  usartEnable   = (USART_CMD_RXEN | USART_CMD_TXEN)
} USART_Enable_TypeDef;

/** Clock polarity/phase mode. */
typedef enum
{
  /** Clock idle low, sample on rising edge. */
  usartClockMode0 = USART_CTRL_CLKPOL_IDLELOW | USART_CTRL_CLKPHA_SAMPLELEADING,

  /** Clock idle low, sample on falling edge. */
  usartClockMode1 = USART_CTRL_CLKPOL_IDLELOW | USART_CTRL_CLKPHA_SAMPLETRAILING,

  /** Clock idle high, sample on falling edge. */
  usartClockMode2 = USART_CTRL_CLKPOL_IDLEHIGH | USART_CTRL_CLKPHA_SAMPLELEADING,

  /** Clock idle high, sample on rising edge. */
  usartClockMode3 = USART_CTRL_CLKPOL_IDLEHIGH | USART_CTRL_CLKPHA_SAMPLETRAILING
} USART_ClockMode_TypeDef;

#if defined(_USART_INPUT_MASK)
/** USART Rx input PRS selection. */
typedef enum
{
  usartPrsRxCh0  = USART_INPUT_RXPRSSEL_PRSCH0,    /**< PRSCH0  selected as USART_INPUT */
  usartPrsRxCh1  = USART_INPUT_RXPRSSEL_PRSCH1,    /**< PRSCH1  selected as USART_INPUT */
  usartPrsRxCh2  = USART_INPUT_RXPRSSEL_PRSCH2,    /**< PRSCH2  selected as USART_INPUT */
  usartPrsRxCh3  = USART_INPUT_RXPRSSEL_PRSCH3,    /**< PRSCH3  selected as USART_INPUT */

#if defined(USART_INPUT_RXPRSSEL_PRSCH7)
  usartPrsRxCh4  = USART_INPUT_RXPRSSEL_PRSCH4,    /**< PRSCH4  selected as USART_INPUT */
  usartPrsRxCh5  = USART_INPUT_RXPRSSEL_PRSCH5,    /**< PRSCH5  selected as USART_INPUT */
  usartPrsRxCh6  = USART_INPUT_RXPRSSEL_PRSCH6,    /**< PRSCH6  selected as USART_INPUT */
  usartPrsRxCh7  = USART_INPUT_RXPRSSEL_PRSCH7,    /**< PRSCH7  selected as USART_INPUT */
#endif

#if defined(USART_INPUT_RXPRSSEL_PRSCH11)
  usartPrsRxCh8  = USART_INPUT_RXPRSSEL_PRSCH8,    /**< PRSCH8  selected as USART_INPUT */
  usartPrsRxCh9  = USART_INPUT_RXPRSSEL_PRSCH9,    /**< PRSCH9  selected as USART_INPUT */
  usartPrsRxCh10 = USART_INPUT_RXPRSSEL_PRSCH10,   /**< PRSCH10 selected as USART_INPUT */
  usartPrsRxCh11 = USART_INPUT_RXPRSSEL_PRSCH11    /**< PRSCH11 selected as USART_INPUT */
#endif
} USART_PrsRxCh_TypeDef;
#endif

/** Synchronous mode init structure. */
typedef struct
{
  /** Specifies whether TX and/or RX shall be enabled when init completed. */
  USART_Enable_TypeDef    enable;

  /**
   * USART/UART reference clock assumed when configuring baudrate setup. Set
   * it to 0 if currently configurated reference clock shall be used.
   */
  uint32_t                refFreq;

  /** Desired baudrate. */
  uint32_t                baudrate;

  /** Number of databits in frame. */
  USART_Databits_TypeDef  databits;

  /** Select if to operate in master or slave mode. */
  bool                    master;

  /** Select if to send most or least significant bit first. */
  bool                    msbf;

  /** Clock polarity/phase mode. */
  USART_ClockMode_TypeDef clockMode;
#if defined(USART_INPUT_RXPRS) && defined(USART_TRIGCTRL_AUTOTXTEN)
  /** Enable USART Rx via PRS. */
  bool                    prsRxEnable;

  /** Select PRS channel for USART Rx. (Only valid if prsRxEnable is true). */
  USART_PrsRxCh_TypeDef   prsRxCh;

  /** Enable AUTOTX mode. Transmits as long as RX is not full.
   *  If TX is empty, underflows are generated. */
  bool                    autoTx;
#endif
#if defined(_USART_TIMING_CSHOLD_MASK)
  /** Auto CS enabling */
  bool                    autoCsEnable;
  /** Auto CS hold time in baud cycles */
  uint8_t                 autoCsHold;
  /** Auto CS setup time in baud cycles */
  uint8_t                 autoCsSetup;
#endif
} USART_InitSync_TypeDef;


/** Default config for USART sync init structure. */
#if defined(_USART_TIMING_CSHOLD_MASK)
#define USART_INITSYNC_DEFAULT                                                              \
{                                                                                           \
  usartEnable,       /* Enable RX/TX when init completed. */                                \
  0,                 /* Use current configured reference clock for configuring baudrate. */ \
  1000000,           /* 1 Mbits/s. */                                                       \
  usartDatabits8,    /* 8 databits. */                                                      \
  true,              /* Master mode. */                                                     \
  false,             /* Send least significant bit first. */                                \
  usartClockMode0,   /* Clock idle low, sample on rising edge. */                           \
  false,             /* Not USART PRS input mode. */                                        \
  usartPrsRxCh0,     /* PRS channel 0. */                                                   \
  false,             /* No AUTOTX mode. */                                                  \
  false,             /* No AUTOCS mode */                                                   \
  0,                 /* Auto CS Hold cycles */                                              \
  0                  /* Auto CS Setup cycles */                                             \
}
#elif defined(USART_INPUT_RXPRS) && defined(USART_TRIGCTRL_AUTOTXTEN)
#define USART_INITSYNC_DEFAULT                                                              \
{                                                                                           \
  usartEnable,       /* Enable RX/TX when init completed. */                                \
  0,                 /* Use current configured reference clock for configuring baudrate. */ \
  1000000,           /* 1 Mbits/s. */                                                       \
  usartDatabits8,    /* 8 databits. */                                                      \
  true,              /* Master mode. */                                                     \
  false,             /* Send least significant bit first. */                                \
  usartClockMode0,   /* Clock idle low, sample on rising edge. */                           \
  false,             /* Not USART PRS input mode. */                                        \
  usartPrsRxCh0,     /* PRS channel 0. */                                                   \
  false              /* No AUTOTX mode. */                                                  \
}
#else
#define USART_INITSYNC_DEFAULT                                                              \
{                                                                                           \
  usartEnable,       /* Enable RX/TX when init completed. */                                \
  0,                 /* Use current configured reference clock for configuring baudrate. */ \
  1000000,           /* 1 Mbits/s. */                                                       \
  usartDatabits8,    /* 8 databits. */                                                      \
  true,              /* Master mode. */                                                     \
  false,             /* Send least significant bit first. */                                \
  usartClockMode0    /* Clock idle low, sample on rising edge. */                           \
}
#endif


//__ramfunc void USART_printHex(uint32_t integer);
__ramfunc void USART_printInt(uint32_t integer);
__ramfunc void USART_txByte(uint8_t data);
__ramfunc uint8_t USART_rxByte(void);
//__ramfunc void USART_printString(uint8_t *string);
void USART_init(void);
void USART_setSync(USART_TypeDef *usart, const USART_InitSync_TypeDef *init);
//void USART_reset(USART_TypeDef *usart);


#endif
