/*
 * Copyright (C) 2016  Stefan Damkjar
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

/**
 * @file brownout.h
 * @author Stefan Damkjar
 * @date 2016-06-28
 */

#ifndef __BROWNOUT_H
#define __BROWNOUT_H

/*******************************************************************************
 *******************************   DEFINES   ***********************************
 ******************************************************************************/

/*****************************************************
 *                                                   *
 *   TRIGGER_VALUE = (VOLTAGE_LEVEL - 1.667)/0.034   *
 *                                                   *
 * ***************************************************/
 #define TRIGGER_LEVEL          (36)

/*******************************************************************************
 ******************************   PROTOTYPES   *********************************
 ******************************************************************************/

void BRNO_Setup(void);

void BRNO_armReset(void);

void BRNO_WaitForVoltageAboveTrigger(void);

#endif  // __BROWNOUT_H
